from LockIn_SR530_GUI import Ui_LockIn_SRS530
from PyQt4 import QtCore
from PyQt4 import QtGui
import sys

#from visa import *
import time

import numpy

#GPIB do equipamento HP4145B tem que estar ajustado para 01



class LockIn_Window(QtGui.QMainWindow, Ui_LockIn_SRS530):
	
	#LockIn = GpibInstrument('GPIB0::1', timeout= 10)
	
	def __init__(self, parent=None):
		super(LockIn_Window, self).__init__(parent)
		
		self.setupUi(self)
		
		#self.StatusByte = LockIn_Window.LockIn.ask("Y") #READING STATUS BYTE and clearing all previous conditions
	
		self.Frequency = 0
		self.Channel1 = 0
		self.Channel2 = 0
		
		# QtCore.QObject.connect(self.SensIncrease_btn,QtCore.SIGNAL("clicked()"), self.SensIncrease_btn )
		# QtCore.QObject.connect(self.SensDecrease_btn,QtCore.SIGNAL("clicked()"), self.SensDecrease_btn )
		# QtCore.QObject.connect(self.DynResIncrease_btn,QtCore.SIGNAL("clicked()"), self.DynResIncrease_btn )
		# QtCore.QObject.connect(self.DynResDecrease_btn,QtCore.SIGNAL("clicked()"), self.DynResDecrease_btn )
		# QtCore.QObject.connect(self.PreTCIncrease_btn,QtCore.SIGNAL("clicked()"), self.PreTCIncrease_btn )
		# QtCore.QObject.connect(self.PreTCDecrease_btn,QtCore.SIGNAL("clicked()"), self.PreTCDecrease_btn )
		# QtCore.QObject.connect(self.PostTCIncrease_btn,QtCore.SIGNAL("clicked()"), self.PostTCIncrease_btn )
		# QtCore.QObject.connect(self.PostTCDecrease_btn,QtCore.SIGNAL("clicked()"), self.PostTCDecrease_btn )
		# QtCore.QObject.connect(self.Bandpass_btn,QtCore.SIGNAL("clicked()"), self.Bandpass_btn )
		# QtCore.QObject.connect(self.LineIn_btn,QtCore.SIGNAL("clicked()"), self.LineIn_btn )
		# QtCore.QObject.connect(self.Linex2_btn,QtCore.SIGNAL("clicked()"), self.Linex2_btn )
		# QtCore.QObject.connect(self.CH1Rel_btn,QtCore.SIGNAL("clicked()"), self.CH1Rel_btn )
		# QtCore.QObject.connect(self.CH1OffsetOnOff_btn,QtCore.SIGNAL("clicked()"), self.CH1OffsetOnOff_btn )
		# QtCore.QObject.connect(self.CH1OffsetUp_btn,QtCore.SIGNAL("clicked()"), self.CH1OffsetUp_btn )
		# QtCore.QObject.connect(self.CH1OffsetDown_btn,QtCore.SIGNAL("clicked()"), self.CH1OffsetDown_btn )
		# QtCore.QObject.connect(self.CH1Expand_btn,QtCore.SIGNAL("clicked()"), self.CH1Expand_btn )
		# QtCore.QObject.connect(self.CH2Rel_btn,QtCore.SIGNAL("clicked()"), self.CH2Rel_btn )
		# QtCore.QObject.connect(self.CH2OffsetOnOff_btn,QtCore.SIGNAL("clicked()"), self.CH2OffsetOnOff_btn )
		# QtCore.QObject.connect(self.CH2OffsetUp_btn,QtCore.SIGNAL("clicked()"), self.CH2OffsetUp_btn )
		# QtCore.QObject.connect(self.CH2OffsetDown_btn,QtCore.SIGNAL("clicked()"), self.CH2OffsetDown_btn )
		# QtCore.QObject.connect(self.CH2Expand_btn,QtCore.SIGNAL("clicked()"), self.CH2Expand_btn )
		# QtCore.QObject.connect(self.DisplayUP_btn,QtCore.SIGNAL("clicked()"), self.DisplayUP_btn )
		# QtCore.QObject.connect(self.DisplayDown_btn,QtCore.SIGNAL("clicked()"), self.DisplayDown_btn )
		# QtCore.QObject.connect(self.DegUP_btn,QtCore.SIGNAL("clicked()"), self.DegUP_btn )
		# QtCore.QObject.connect(self.DegDown_btn,QtCore.SIGNAL("clicked()"), self.DegDown_btn )
		# QtCore.QObject.connect(self.For2F_btn,QtCore.SIGNAL("clicked()"), self.For2F_btn )
		# QtCore.QObject.connect(self.B90UP_btn,QtCore.SIGNAL("clicked()"), self.B90UP_btn )
		# QtCore.QObject.connect(self.B90Down_btn,QtCore.SIGNAL("clicked()"), self.B90Down_btn )
		# QtCore.QObject.connect(self.Trig_btn,QtCore.SIGNAL("clicked()"), self.Trig_btn )
		# QtCore.QObject.connect(self.SelectReference_btn,QtCore.SIGNAL("clicked()"), self.SelectReference_btn )
		
		
		
		# call the update method (to speed-up visualization)
		self.timerEvent(None)

		# start timer, trigger event every 100 millisecs (=0.1sec)
		self.timer = self.startTimer(100)  #QTimer

	# def SelectReference_btn(self):
		# LockIn_Window.LockIn.write("K 5")
	# def DisplayUP_btn(self):
		# LockIn_Window.LockIn.write("K 18")
	# def DisplayDown_btn(self):
		# LockIn_Window.LockIn.write("K 19")
	# def DegUP_btn(self):
		# LockIn_Window.LockIn.write("K 11")
	# def DegDown_btn(self):
		# LockIn_Window.LockIn.write("K 12")
	# def For2F_btn(self):
		# LockIn_Window.LockIn.write("K 10")
	# def B90UP_btn(self):
		# LockIn_Window.LockIn.write("K 6")
	# def B90Down_btn(self):
		# LockIn_Window.LockIn.write("K 7")
	# def Trig_btn(self):
		# LockIn_Window.LockIn.write("K 9")	
	# def Bandpass_btn(self):
		# LockIn_Window.LockIn.write("K 32")
	# def LineIn_btn(self):
		# LockIn_Window.LockIn.write("K 31")
	# def Linex2_btn(self):
		# LockIn_Window.LockIn.write("K 30")
	# def CH1Rel_btn(self):
		# LockIn_Window.LockIn.write("K 21")
	# def CH1OffsetOnOff_btn(self):
		# LockIn_Window.LockIn.write("K 22")
	# def CH1OffsetUp_btn(self):
		# LockIn_Window.LockIn.write("K 23")
	# def CH1OffsetDown_btn(self):
		# LockIn_Window.LockIn.write("K 24")
	# def CH1Expand_btn(self):
		# LockIn_Window.LockIn.write("K 20")	
	# def CH2Rel_btn(self):
		# LockIn_Window.LockIn.write("K 13")
	# def CH2OffsetOnOff_btn(self):
		# LockIn_Window.LockIn.write("K 14")
	# def CH2OffsetUp_btn(self):
		# LockIn_Window.LockIn.write("K 15")
	# def CH2OffsetDown_btn(self):
		# LockIn_Window.LockIn.write("K 16")
	# def CH2Expand_btn(self):
		# LockIn_Window.LockIn.write("K 17")
	# def SensIncrease_btn(self):
		# LockIn_Window.LockIn.write("K 27")
	# def REPESensDecrease_btnAT(self):
		# LockIn_Window.LockIn.write("K 28")
	# def DynResIncrease_btn(self):
		# LockIn_Window.LockIn.write("K 25")
	# def DynResDecrease_btn(self):
		# LockIn_Window.LockIn.write("K 26")
	# def PreTCIncrease_btn(self):
		# LockIn_Window.LockIn.write("K 3")
	# def PreTCDecrease_btn(self):
		# LockIn_Window.LockIn.write("K 4")
	# def PostTCIncrease_btn(self):
		# LockIn_Window.LockIn.write("K 1")
	# def PostTCDecrease_btn(self):
		# LockIn_Window.LockIn.write("K 2")

	def timerEvent(self, evt):
		"""Custom timerEvent code, called at timer event receive"""
		#self.Frequency = float(LockIn_Window.LockIn.ask("F"))
		self.Frequency = 154.32
		self.Reference_lcd.display(self.Frequency)
		
		#self.Channel1 = float(LockIn_Window.LockIn.ask("Q1"))
		self.Channel1 = 0.000001
		self.Channel1_lcd.display(self.Channel1 )
		
		#self.Channel2 = float(LockIn_Window.LockIn.ask("Q2"))
		self.Channel2 = 123.36456
		self.Channel2_lcd.display(self.Channel2 )
		
		# self.StatusBit2 = LockIn_Window.LockIn.ask("Y 2") #This bit is set when no reference input is detected
		# self.StatusBit3 = LockIn_Window.LockIn.ask("Y 3") #This bit is set when the reference oscillator is not locked to the reference input.
		# self.StatusBit4 = LockIn_Window.LockIn.ask("Y 4") #This bit is set if there is a signal overload
		self.OVLD_txt.setText("<font style='color: black;background: red;'>OVLD</font>") #overload ON
		self.OVLD_txt.setText("<font style='color: gray;'>OVLD</font>") #Overload OFF
		self.OVLD_txt.setEnabled(False)
		self.UNLK_txt.setText("<font style='color: black;background: red;'>UNLK</font>")
		self.UNLK_txt.setEnabled(True)
		
app = QtGui.QApplication(sys.argv)

dmw = LockIn_Window()
dmw.show()

#devlist = get_instruments_list()
#print "Lista de Instrumentos: %s" %devlist

# for instrumento in devlist:
    # my_instrument = instrument(instrumento, term_chars = "\n")
    # try:
        # my_instrument.write("*IDN?")
        # print instrumento, ' - ', my_instrument.read()
    # except:
        # print "O instrumento %s nao respondeu ao comando *IDN?. Tentando o comando INFO?" %instrumento
        # my_instrument.write("INFO?")
        # print instrumento, ' - ', my_instrument.read()
        # pass







sys.exit(app.exec_())